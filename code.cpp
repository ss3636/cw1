#include<bits/stdc++.h>
using namespace std;

int lm, suc, f;
// lm  -> live music
// suc -> standup comedy
// f   -> film

int stand_cap;
int seat_cap;

// stand_cap -> standing capacity
// seat_cap  -> seating capacity

void init(){

    // Given initial capacity
    stand_cap = 300;
    seat_cap = 200;

    lm = 0;
    suc = 0;
    f = 0;
}

void add(){

    int input,n;

    cout<<"Choose the event you want to book -> "<<endl;
    cout<<" 1 -> Live Music \n 2 -> Standup comedy\n 3 -> Film\n";
    cin>>input;

    cout<<"Enter number of tickets you want\n";
    cin>>n;

    switch(input){

        case 1 : {
            if (n <= stand_cap){
                stand_cap -= n;
                lm += n;
            }

            else
                cout<<"Insufficient Capacity\n";

            break;
        }

        case 2: {
            if (n <= seat_cap){
                seat_cap -= n;
                suc += n;
            }

            else
                cout<<"Insufficient Capacity\n";

            break;
        }

        case 3: {
            if (n <= seat_cap){
                seat_cap -= n;
                f += n;
            }

            else
                cout<<"Insufficient Capacity\n";

            break;
        }

        default: {
            cout<<" No such Option available\n";
            break;
        }
    }
}

void cancel(){
    int input,n;

    cout<<"Choose the event you want to cancel -> "<<endl;
    cout<<" 1 -> Live Music \n 2 -> Standup comedy\n 3 -> Film\n";
    cin>>input;

    cout<<"Enter number of tickets you want to cancel ( // Assuming this would always be equal to or less than the number of tickets booked by a user //)\n";
    cin>>n;

    switch(input){

        case 1 : {

            stand_cap += n;
            lm -= n;

            break;
        }

        case 2: {

            seat_cap += n;
            suc -= n;

            break;
        }

        case 3: {

            stand_cap += n;
            f -= n;

            break;
        }

        default: {
            cout<<" No such Option available\n";
            break;
        }
    }
}

void listevents(){

    cout<<"Number of tickets booked are -> "<<endl;
    cout<<"Seated -> "<<(suc+f)<<endl;
    cout<<"Standing -> "<<lm<<endl;

}

void specificlist(){
    int input,n;

    cout<<"Choose the event you want to Check -> "<<endl;
    cout<<" 1 -> Live Music \n 2 -> Standup comedy\n 3 -> Film\n";
    cin>>input;

    switch(input){

        case 1 : {

            cout<<"Tickets Booked -> "<<lm<<endl;
            cout<<"Standing capacity left -> "<<stand_cap<<endl;
            break;
        }

        case 2: {

            cout<<"Tickets Booked -> "<<suc<<endl;
            cout<<"Seating capacity left -> "<<seat_cap<<endl;
            break;
        }

        case 3: {

            cout<<"Tickets Booked -> "<<f<<endl;
            cout<<"Seating capacity left -> "<<seat_cap<<endl;
            break;
        }

        default: {
            cout<<" No such Option available\n";
            break;
        }
    }
}

int main(){

    cout<<" ------------------  Theatre management program  ---------------------- ";
    cout<<endl;

    init();

    int input;
    bool flag = true;

    while(flag == true){
        cout<<"Choose Your Option - "<<endl;
        cout<<" 1 -> add booking \n 2 -> cancel booking \n 3 -> List events \n 4 -> List Details of given event \n 5 -> End console \n\n";
        cin>>input;

        switch(input){

            case 1 : {
                add();
                break;
            }

            case 2: {
                cancel();
                break;
            }

            case 3: {
                listevents();
                break;
            }

            case 4: {
                specificlist();
                break;
            }

            case 5: {
                flag = false;
                cout<< " ---------------  END  ----------------- "<<endl;
                break;
            }

            default: {
                cout<<" No such Option available\n";
                break;
            }
        }
    }

    return 0;
}
